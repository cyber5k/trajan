from django.db import models
from django.utils import timezone
#from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.urls import reverse

User = get_user_model()

# Create your models here.

class PublishedManager(models.Manager):
    """
    Manager for published posts.
    """

    def get_queryset(self):
        return super(PublishedManager,
                     self).get_queryset().filter(status='published')

class Post(models.Model):
    """
    Blog post.
    """

    objects = models.Manager() # default manager
    published = PublishedManager() # custom manager

    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published'),
    )

    CATEGORIES = (
        ('church', 'Church'),
        ('computer', 'Computer'),
        ('engineering', 'Engineering'),
    )

    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250,
                            unique_for_date='publish')
    author = models.ForeignKey(User,
                               on_delete=models.CASCADE,
                               related_name='blog_posts')
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10,
                              choices=STATUS_CHOICES,
                              default='draft')
    category = models.CharField(max_length=20,
                                choices=CATEGORIES,
                                default='engineering')

    class Meta:
        ordering = ('-publish',)

    def __repr__(self):
        return self.title

    def get_absolute_url(self):
        """
        Absolute URL for a post
        """
        return reverse('blog:post_detail',
                       args=[self.publish.year,
                             self.publish.month,
                             self.publish.day,
                             self.slug])
