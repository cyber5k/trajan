from django.db import models

from home.models import Persona, EncKeyPair

# Create your models here.
enc_choices = (
    ('AES-256-CBC-1', 'Default Advanced Encryption Standard 256-bit'),
)
enc_default = 'AES-256-CBC-1'

class RoomKey(models.Model):
    #room = models.ForeignKey(Room,
    #                        related_name='roomkey',
    #                        on_delete=models.CASCADE)
    
    ekp = models.ForeignKey(EncKeyPair,
                            related_name='roomkey',
                            on_delete=models.CASCADE)

    enckey = models.CharField(max_length=10000)

    encgroup = models.CharField(max_length=500)
    encgroup_enctype = models.CharField(max_length=50,
                                        choices=enc_choices,
                                        default=enc_default)
    
    encname = models.CharField(max_length=500)
    encname_enctype = models.CharField(max_length=50,
                                        choices=enc_choices,
                                        default=enc_default)
    
    #class Meta:
    #    unique_together = (
    #        ('room', 'ekp'),
    #    )
