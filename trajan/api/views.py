from django.shortcuts import render
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
User = get_user_model()

from django.http import HttpResponse, HttpResponseForbidden, JsonResponse
from django.contrib.auth.decorators import login_required

from rest_framework import generics, viewsets, status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from api.serializers import PersonaSerializer, UserSerializer, EncKeyPairSerializer, SignKeyPairSerializer, GroupSerializer, RoomKeySerializer

from api.permissions import IsOwnerOrReadOnly

from django.core import serializers
from home.models import Persona, EncKeyPair, SignKeyPair
from chat.models import ClientChannel
from chat.consumers import ChatConsumer
from .models import RoomKey

# Create your views here.
class UserList(generics.ListAPIView):
    #queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return User.objects.filter(pk=self.request.user.pk)

class UserDetail(generics.RetrieveAPIView):
    #queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    
    def get_queryset(self):
        return User.objects.filter(pk=self.request.user.pk)

class RoomKeyList(generics.ListCreateAPIView):
    """
    Room Key List view
    """
    serializer_class = RoomKeySerializer
    permission_classes = (IsAuthenticated,
                          IsOwnerOrReadOnly,)

    def get_queryset(self):
        return RoomKey.objects.filter(ekp__in=EncKeyPair.objects.filter(persona__in=Persona.objects.filter(owner=self.request.user)))
    
    # this is where users can setup roomkeys with other people's EncKeyPairs
    def perform_create(self, serializer):
        #ekp = EncKeyPair.objects.filter(persona__in=Persona.objects.filter(owner=self.request.user)).get(pk=serializer.validated_data['ekp'])
        ekp = EncKeyPair.objects.get(pk=serializer.validated_data['ekp'])

        # ekp is owned by self.request.user
        if ekp in EncKeyPair.objects.filter(persona__in=Persona.objects.filter(owner=self.request.user)):
            # saving key for self
            rk = serializer.save(ekp=ekp)
            
        # ekp is not owned by self.request.user (so don't save roomkey, but send invitation)
        else:    
            # send notification to other to load room
            otherUser = User.objects.filter(persona__in=Persona.objects.filter(enckeypair=ekp))
            for u in otherUser:
                #print('Send key to {}'.format(u))
                clientChannels = ClientChannel.objects.filter(client=u)
                if len(clientChannels) > 0:
                    # user is online and can accept or reject invitation
                    #rk = serializer.save(ekp=ekp)
                    for c in clientChannels:
                        cn = c.channel_name
                        ChatConsumer.send_notification(cn,
                            {
                                'action': 'invite',
                                'inviter': self.request.user.username,
                                'roomkey': serializer.data,
                            },
                        )
                else:
                    # invited user is not online: do not save, and send rejection notification to sender
                    userChannels = ClientChannel.objects.filter(client=self.request.user)
                    for c in userChannels:
                        cn = c.channel_name
                        ChatConsumer.send_notification(cn,
                            {
                                'action': 'reject',
                                'description': 'user is not online',
                            },
                        )

class RoomKeyDetail(generics.RetrieveDestroyAPIView):
    """
    Room Key Detail view
    """
    serializer_class = RoomKeySerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return RoomKey.objects.filter(ekp__in=EncKeyPair.objects.filter(persona__in=Persona.objects.filter(owner=self.request.user)))

class PersonaList(generics.ListCreateAPIView):
    """
    Personas List view
    """
    #queryset = Persona.objects.all()
    serializer_class = PersonaSerializer
    permission_classes = (IsAuthenticated,
                          IsOwnerOrReadOnly,)
    
    def get_queryset(self, *args, **kwargs):
        #return self.request.user.persona_set.all()
        return Persona.objects.filter(owner=self.request.user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class PersonaDetail(generics.RetrieveDestroyAPIView):
    """
    Persona Detail view
    """
    #queryset = Persona.objects.all()
    serializer_class = PersonaSerializer
    permission_classes = (IsAuthenticated,
                          IsOwnerOrReadOnly,)
    
    def get_queryset(self, *args, **kwargs):
    #    return self.request.user.persona_set.all()
        return Persona.objects.filter(owner=self.request.user)

class EncKeyPairList(generics.ListCreateAPIView):
    """
    Encryption Key Pair List view
    """
    serializer_class = EncKeyPairSerializer
    permission_classes = (IsAuthenticated,
                          IsOwnerOrReadOnly,)


    def get_queryset(self):
        return EncKeyPair.objects.filter(persona__in=Persona.objects.filter(owner=self.request.user))

    def perform_create(self, serializer):
        persona = Persona.objects.filter(owner=self.request.user).filter(pk=self.request.data['persona']).first()
        
        if not persona:
            raise serializers.ValidationError('Bad persona')
        
        serializer.save(persona=persona)

class EncKeyPairDetail(generics.RetrieveAPIView):
    """
    Encryption Key Pair Detail view
    """
    serializer_class = EncKeyPairSerializer
    permission_classes = (IsAuthenticated,
                          IsOwnerOrReadOnly,)


    def get_queryset(self):
        return EncKeyPair.objects.filter(persona__in=Persona.objects.filter(owner=self.request.user))


class SignKeyPairList(generics.ListCreateAPIView):
    """
    Signing Key Pair List view
    """
    serializer_class = SignKeyPairSerializer
    permission_classes = (IsAuthenticated,
                          IsOwnerOrReadOnly,)


    def get_queryset(self):
        return SignKeyPair.objects.filter(persona__in=Persona.objects.filter(owner=self.request.user))
    
    def perform_create(self, serializer):
        persona = Persona.objects.filter(owner=self.request.user).filter(pk=self.request.data['persona']).first()
        
        if not persona:
            raise serializers.ValidationError('Bad persona')
        
        serializer.save(persona=persona)

class SignKeyPairDetail(generics.RetrieveAPIView):
    """
    Signing Key Pair Detail view
    """
    serializer_class = SignKeyPairSerializer
    permission_classes = (IsAuthenticated,
                          IsOwnerOrReadOnly,)


    def get_queryset(self):
        return SignKeyPair.objects.filter(persona__in=Persona.objects.filter(owner=self.request.user))

#class RoomList(generics.ListCreateAPIView):
#    """
#    List Rooms
#    """
#    #queryset = Room.objects.all()
#    serializer_class = RoomSerializer
#    permission_classes = (IsAuthenticated,)
#        
#    def get_queryset(self):
#        return Room.objects.filter(owner__in=Persona.objects.filter(owner=self.request.user))
#    
#    def perform_create(self, serializer):
#        persona = Persona.objects.filter(owner=self.request.user).get(pk=serializer.data['persona'])
#        serializer.save(owner=persona)
#
#class RoomDetail(generics.RetrieveDestroyAPIView):
#    """
#    Detailed Room view
#    """
#    #queryset = Room.objects.all()
#    serializer_class = RoomSerializer
#    permission_classes = (IsAuthenticated,)
#
#    def get_queryset(self):
#        return Room.objects.filter(owner__in=Persona.objects.filter(owner=self.request.user))

##### UNUSED #######

def room_list(request):

    if not request.user.is_authenticated:
        return HttpResponseForbidden('Unauthenticated')

    if request.method == 'GET':
        rooms = Room.objects.all()
        serializer = RoomSerializer(rooms, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = RoomSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

    return HttpResponseForbidden('Unsuccessful')

def room_details(request, pk):
    
    if not request.user.is_authenticated:
        return HttpResponseForbidden('Unauthenticated')

    try:
        room = Room.objects.get(pk=pk)
    except Room.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = RoomSerializer(room)
        return JsonResponse(serializer.data)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = RoomSerializer(room, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        room.delete()
        return HttpResponse(status=204)

    return HttpResponseForbidden('Unsuccessful')

