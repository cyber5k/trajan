import logging
#from django.contrib.auth.models import (
#    AbstractUser,
#    AbstractBaseUser,
#    BaseUserManager,
#)
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import Count, Sum
from django.contrib.auth import get_user_model

from . import exceptions

User = get_user_model()

logger = logging.getLogger(__name__)

#class UserManager(BaseUserManager):
#    use_in_migrations = True
#
#    def _create_user(self, email, password, **extra_fields):
#        if not email:
#            raise ValueError("The given email must be set")
#        email = self.normalize_email(email)
#        user = self.model(email=email, **extra_fields)
#        user.set_password(password)
#        user.save(using=self._db)
#        return user
#
#    def create_user(self, email, password=None, **extra_fields):
#        #extra_fields.setdefault("is_staff", False)
#        #extra_fields.setdefault("is_superuser", False)
#        return self._create_user(email, password, **extra_fields)
#
#    def create_superuser(self, email, password, **extra_fields):
#        extra_fields.setdefault("is_staff", True)
#        extra_fields.setdefault("is_superuser", True)
#
#        if extra_fields.get("is_staff") is not True:
#            raise ValueError(
#                "Superuser must have is_staff=True."
#            )
#        if extra_fields.get("is_superuser") is not True:
#            raise ValueError(
#                "Superuser must have is_superuser=True."
#            )
#
#        return self._create_user(email, password, **extra_fields)
#
#
#class User(AbstractUser):
#    username = None
#    email = models.EmailField("email address", unique=True)
#
#    date_joined = models.DateTimeField('dateJoined', auto_now_add=True)
#    is_staff = models.BooleanField('staff', default=False)
#
#    USERNAME_FIELD = "email"
#    REQUIRED_FIELDS = []
#
#    objects = UserManager()
#
#    #@property
#    #def is_employee(self):
#    #    return self.is_active and (
#    #        self.is_superuser
#    #        or self.is_staff
#    #        and self.groups.filter(name="Employees").exists()
#    #    )
#
#    #@property
#    #def is_dispatcher(self):
#    #    return self.is_active and (
#    #        self.is_superuser
#    #        or self.is_staff
#    #        and self.groups.filter(name="Dispatchers").exists()
#    #    )


class PQC_demo(models.Model):
    pubkey = models.CharField(max_length=10000)
    privkey = models.CharField(max_length=10000)
    plaintext = models.CharField(max_length=100)
    ciphertext = models.CharField(max_length=10000)

class Persona(models.Model):
    owner = models.ForeignKey(User,
                              related_name='persona',
                              on_delete=models.CASCADE)
    name = models.CharField(max_length=50,
                            unique=True)

class KeyPair(models.Model):
    
    privkey_enc_choices = (
        ('AES-256-CBC-1', 'Default Advanced Encryption Standard 256-bit'),
    )
    privkey_enc_default = 'AES-256-CBC-1'
    
    pubkey = models.CharField(max_length=10000,
                              unique=True)
    encprivkey = models.CharField(max_length=10000)
    privkey_enctype = models.CharField(max_length=50,
                                       choices=privkey_enc_choices,
                                       default=privkey_enc_default)

    #class Meta:
    #    abstract = False 


class EncKeyPair(KeyPair):
    name = models.CharField(max_length=50,
                            choices=(
                                ('NTRU-256-default-1', 'Default NTRU 256-bit'),
                                ('RSA-4096', 'RSA 4096 bit modulus'),
                            ),
                            default='NTRU-256-default-1')
    
    persona = models.ForeignKey(Persona,
                                related_name='enckeypair',
                                on_delete=models.CASCADE)

class SignKeyPair(KeyPair):
    name = models.CharField(max_length=50,
                            choices=(
                                ('RSA-4096', 'RSA 4096 bit modulus'),
                            ),
                            default='RSA-4096')
    
    persona = models.ForeignKey(Persona,
                                related_name='signkeypair',
                                on_delete=models.CASCADE)

class Signature(models.Model):
    signing_key = models.ForeignKey(SignKeyPair,
                                    related_name="signing_key",
                                    on_delete=models.CASCADE)
    
    signature_type = models.CharField(max_length=50,
                                     choices=(
                                        ('RSA-sha256-default-1','RSA SHA256 default'),
                                     ),
                                     default='RSA-sha256-default-1')
    
    signature = models.CharField(max_length=500)

    class Meta:
        abstract = True

class KeySignature(Signature):
    key = models.ForeignKey(KeyPair,
                            related_name="signed_key",
                            on_delete=models.CASCADE)

