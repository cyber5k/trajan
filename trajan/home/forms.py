
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth import get_user_model
from captcha.fields import CaptchaField

from .models import PQC_demo 

User = get_user_model()

# integrate with allauth
from allauth.account.forms import LoginForm
class AllauthCompatLoginForm(LoginForm):
    def user_credentials(self):
        credentials = super(AllauthCompatLoginForm, self).user_credentials()
        credentials['login'] = credentials.get('email') or credentials.get('username')
        return credentials


class CustomUserCreationForm(UserCreationForm):
    
    captcha = CaptchaField()
    class Meta(UserCreationForm.Meta):
        model = User
        #fields = UserCreationForm.Meta.fields
        fields = ('email',)

class CustomUserChangeForm(UserChangeForm):
    
    captcha = CaptchaField
    class Meta:
        model = User
        #fields = UserChangeForm.Meta.fields
        fields = ('email',)

class PQCdemoForm(forms.ModelForm):

    class Meta:
        model = PQC_demo 
        widgets = {
            'pubkey': forms.Textarea(attrs={'rows': 15, 'cols': 80}),
            'privkey': forms.Textarea(attrs={'rows': 4, 'cols': 80}),
            'plaintext': forms.Textarea(attrs={'rows': 4, 'cols': 80}),
            'ciphertext': forms.Textarea(attrs={'rows': 4, 'cols': 80}),
        }
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        
        import valens
        V = valens.Valens()
        
        initial = kwargs.pop('initial', {})
        initial['pubkey'] = V.getPubKey()
        initial['privkey'] = V.getPrivKey()
        initial['plaintext'] = 'The lazy brown fox jumped over the fence.'
        kwargs['initial'] = initial

        super(PQCdemoForm, self).__init__(*args, **kwargs)

        self.fields['plaintext'].required = False
        self.fields['ciphertext'].required = False


