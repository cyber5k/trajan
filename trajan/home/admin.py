from datetime import datetime, timedelta
import logging
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django import forms
from django.utils.html import format_html
from django.db.models.functions import TruncDay
from django.db.models import Avg, Count, Min, Sum
from django.urls import path
from django.template.response import TemplateResponse
from django.shortcuts import get_object_or_404, render

from django.http import HttpResponse
from django.template.loader import render_to_string
from weasyprint import HTML
import tempfile

#from . import models
from django.contrib.auth import get_user_model
from .forms import CustomUserCreationForm, CustomUserChangeForm

User = get_user_model()

from . import models as home_models

logger = logging.getLogger(__name__)

class CustomUserAdmin(UserAdmin):
    
    model = User
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm

    #fieldsets = (
    #    (None, {'fields': ('username',)}),
    #    (_('Personal info'), {'fields': ('email',)}),
    #)

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Permissions', {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide'),
            'fields': ('email', 'password1', 'password2'),
        }),
    )

    ordering = ['email']

#admin.site.register(User, CustomUserAdmin)
admin.site.register(home_models.Persona)
admin.site.register(home_models.EncKeyPair)
admin.site.register(home_models.SignKeyPair)
