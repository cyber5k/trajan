

$( document ).ready(function() {
    $('#pqc_gen_btn').click(function() {
        var data = $('#pqc_demo_form').serialize();
        $.post('/ajax/pqc_demo_gen/', data, function( newdata ) {
            $('#id_pubkey').html(newdata['content']['pubkey']);
            $('#id_privkey').html(newdata['content']['privkey']);

        });
    });

    $('#pqc_enc_btn').click(function() {
        var data = $('#pqc_demo_form').serialize();
        $.post('/ajax/pqc_demo_enc/', data, function( newdata) {
            $('#id_ciphertext').html(newdata['content']['ciphertext']);
            $('#id_plaintext').html('');
        
        });
    });

    $('#pqc_dec_btn').click(function() {
        var data = $('#pqc_demo_form').serialize();
        $.post('/ajax/pqc_demo_dec/', data, function( newdata ) {
            $('#id_plaintext').html(newdata['content']['plaintext']);
            $('#id_ciphertext').html('');
        });
    });
});
